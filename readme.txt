Titre du projet : Systeme de stationnement automatique

Description : 
	Lors d'un stationnement, l'utilisateur peut activer un système de stationnement automatique
	le système se base sur des données d'entrée pour identifier :
	
		- la place de parking à partir des données de la "camera"
			* Version simplifié : l'utilisateur choisit un type de parking (creneau, bataille, etc) ceci va correspondre un algo de manoeuvre spécifique
		  
		- les obstacles à proximité de la place de parking à partir des données du "capteur de proximité"
			* version simplifié : le capteur sera simulé par un génerateur d'une variable de type 
		
		- les obstacles environants (autres véhicules de passage, piétons, etc..)à partir des données du Radar
			* version simplifié : le capteur sera simulé par un génerateur d'une variable de type
		
	le système actionne les sorties pour effectue la manoeuvre de stationnement : 
		- Tant qu'un obstacle environants est détecté le systeme active le frein, le véhicule reste à l'arret
		  - Sinon : 
			 - le véhicule engage la manoeuvre (angle_volant, pression_acc)
			 - Si un obstacle est detecté à proximité du vehicule,le systeme active le frein, le véhicule reste à l'arret
			 - L'utilisateur peut demander la poursuite de la manoeuvre ou la fin de la manoeuvre
			    * version simplifié : - lorsque l'utilisateur demande la poursuite de la manoeuvre le generateur simulant le capteur de proximité est re-initialisé
	
	A chaque instant l'etat de la voiture + les défauts en cours sont afficher une IHM  : 
			Etats : Debut manoeuvre, Manoeuvre en pause, Manouevre en cours, fin manoeuvre
			Défaut : Detection obstacle environant, Detection obstacle de proximité
			
	Une barre d'etat permet de visualiser l'evolution de la manoeuvre	
	les entrees de l'utilisateur sont effectues aussi à partir de cette IHM

Entrées : 
	
	-  start_stop : [boolean] - True : active le système, False : Stop le système  
	-  parking_type : [enum] - Creneau, Bataille 
	-	dist_proxi : [int] min : 0, max = 10 cm 
	-	dis_envi   :  [int] min : 0, max = 3 m 

Sorties : 

	- angle_volant : 
	- pression_acc : 
	- pression_frein : 
	
algo/threads : 

   -  detection_env() : 
		 - tread qui lie en permanence l'entree dis_envi
		 - si dis_envi < Min_dis_env : met la variable system_state_defaut_envi = 1 [detection defaut obstacle environant]
		 sinon met la variable system_state_defaut_envi = 0 [detection defaut obstacle environant]
		 
   -  detection_prox() : 
		- tread qui lie en permanence l'entree dis_envi
		- si dis_proxi < Min_dis_env : met la variable system_state_defaut_proxi = 1 [detection defaut obstacle environant]
		sinon met la variable system_state_defaut_proxi = 0 [detection defaut obstacle environant]

   -  systeme_state() : 
		- initialiser la variable systeme_state_var = 0 ["debut manoeuvre"]
		- Ecrire systeme_state_var sur l'IHM
		 
		- Tant que non(manouvre_end) : 
			si (system_state_defaut_envi || system_state_defaut_proxi) :  
				Ecrire systeme_state_var = 1 ["Manoeuvre en pause"]
				Ecrire manoueuvre_var = 0 // stop de la manoeuvre
				
			sinon si non((manouvre_end))
				Ecrire systeme_state_var = 2 ["Manoeuvre en cours"]
			
			sinon 
				Ecrire systeme_state_var = 3 ["fin manoeuvre"]
		
   
   -  manoeuvre() : 
		
		-  initialiser timer_t = 0;
		
		-  Tant que(timer_t<max_temp) : 
			Si(manoeuvre_var) : 
				Incrementer (angle_volant)   // variable en fonction du type de manoeuvre
				Incrementer (pression_acc)  // variable en fonction du type de manoeuvre
				Incrementer (Bar_etat)
				Incrementer (timer_t)
			Sinon : 
				Ecrire pression_frein = 1
				Mis_en_default (Bar_etat)  

				
		- Ecrire angle_volant = 0
		- Ecrire pression_acc = 0
		- Ecrire manouvre_end = 1
		
	

>>>>>>> 9663dab70d551c3727d76fd312fe63475c00329e
