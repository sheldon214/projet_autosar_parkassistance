#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#define sleep_sys_state 10
#define sleep_maneouvre 15
/* variable globale */

int dist_proxi = 100;
int dist_envi  = 20;
int system_state_default_proxi = 0;
int system_state_default_envi = 0;
int systeme_state_var = 0;
int manoeuvre_end = 0;
int manoeuvre_var = 0;

pthread_mutex_t lock;

/* thread de detection des obstacles environnant (envi*) du vehicule*/
void* detect_envi(void* arg){

    while(!manoeuvre_end){

        pthread_mutex_lock(&lock);
        printf("Entrer [distance envi] : \n");
        pthread_mutex_unlock(&lock);

        scanf("%d",&dist_envi);

        pthread_mutex_lock(&lock);
        printf("distance_envi : %d\n",dist_envi);
        pthread_mutex_unlock(&lock);



        if(dist_envi<50) system_state_default_envi = 1;

        else system_state_default_envi =0;
    }

    pthread_exit(NULL);
}

/* thread de detection des obstacles a proximiter du vehicule*/
void* detect_proxi(void* arg)
{

    while(!manoeuvre_end){

        pthread_mutex_lock(&lock);
        printf("Entrer [distance proxi] : \n");
        pthread_mutex_unlock(&lock);

        scanf("%d",&dist_proxi);

        pthread_mutex_lock(&lock);
        printf("distance_prox : %d\n",dist_proxi);
        pthread_mutex_unlock(&lock);

        if(dist_proxi<10) system_state_default_proxi = 1;


        else system_state_default_proxi =0;
    }
    pthread_exit(NULL);
}

/* thread de commande des sorties (angle volant, pression acc, pression frein) */
void* process(void* arg)
{
    int count_timer = 0;
    int max_timer = 5;

    pthread_mutex_lock(&lock);
    printf("starting process \n");
    pthread_mutex_unlock(&lock);

    while (count_timer<max_timer){

        if(manoeuvre_var){

            //actualiser(angle_volant,cout_timer);
            pthread_mutex_lock(&lock);
            printf("Manoeuvre : angle volant = 10, pression_acc = 15\n");
            pthread_mutex_unlock(&lock);

            sleep(sleep_maneouvre);

            count_timer++;
        }
        else
        {
            pthread_mutex_lock(&lock);
            printf("Manoeuvre : angle volant = 0, pression_acc = 0, pression_frein = 10\n");
            pthread_mutex_unlock(&lock);
            sleep(sleep_maneouvre);
        }
    }
    pthread_mutex_lock(&lock);
    printf("Ending process \n");
    pthread_mutex_unlock(&lock);

    manoeuvre_end = 1;

    pthread_exit(NULL);
}

/* thread principale calcul de l'etat du systeme */
int main()
{


    pthread_t ptid[3];

    // Demarrage du syst�me (� adapter)
    int start_stop=0;
    do{
        printf("Start ? : ");
        scanf("%d",&start_stop);
    }while(!start_stop);


    // Demarrage des threads IO et Manoeuvre
    pthread_create(&ptid[0], NULL, &detect_envi,NULL);
    pthread_create(&ptid[1], NULL, &detect_proxi,NULL);
    pthread_create(&ptid[1], NULL, &manoeuvre,NULL);



    while(!manoeuvre_end){

        if(system_state_default_proxi || system_state_default_envi){
            pthread_mutex_lock(&lock);
            printf("Systeme state : Manoeuvre en pause\n");
            pthread_mutex_unlock(&lock);
            manoeuvre_var = 0;
            sleep(sleep_sys_state);
        }

        else if(!manoeuvre_end) {

            pthread_mutex_lock(&lock);
            printf("Systeme state : Manoeuvre en cours\n");
            pthread_mutex_unlock(&lock);

            manoeuvre_var = 1;
            sleep(sleep_sys_state);
        }

        else
        {
            pthread_mutex_lock(&lock);
            printf("Systeme state : Manoeuvre termine\n");
            pthread_mutex_unlock(&lock);
        }

    }

    // Waiting for the created thread to terminate
    pthread_join(ptid[0], NULL);
    pthread_join(ptid[1], NULL);
    pthread_join(ptid[2], NULL);

    printf("thread ends\n");
    return 0;
}
