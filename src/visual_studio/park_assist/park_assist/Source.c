/********
Projet : Park assist








*********/


#include "CCL/CCL.h"
#define HAVE_STRUCT_TIMESPEC
#include <pthread.h>

#define WIN32_LEAN_AND_MEAN             // Exclure les en-t�tes Windows rarement utilis�s
// Fichiers d'en-t�te Windows
#include <windows.h>

#define MIN_DIST_PROXI  50
#define MIN_DIST_ENVI   5
#define PROCESS_SLEEP 15
#define THREAD_SLEEP 100

/* declaration des fonctions principales*/
extern void OnMeasurementPreStart();
extern void OnMeasurementStart();
extern void OnMeasurementStop();


extern void OnSysVar_start(int64_t time, int32_t sysVarID);
extern void OnSysVar_Dproxi(int64_t time, int32_t sysVarID);
extern void OnSysVar_Dradar(int64_t time, int32_t sysVarID);
extern void OnTimer1(int64_t time, int32_t timerID);
extern void OnTimer2(int64_t time, int32_t timerID);


/* declarations des ID des variables systemes*/
int32_t gSysVarID_start;
int32_t gSysVarID_Dproxi;
int32_t gSysVarID_Dradar;
int32_t gSysVarID_volant;
int32_t gSysVarID_acc;
int32_t gSysVarID_frein;
int32_t gSysVarID_etat;
int32_t gSysVarID_duree;
int32_t gTimerID1;
int32_t gTimerID2;

/* declarations de l'ID thread principale */
pthread_t ptid_start;

/* creation d'une structure data pour l'ecriture des donnees de sorties/ lecture des donnes d'entree */
typedef struct data {
	//sorties
	int32_t duree;
	int32_t etat;
	int32_t volant;
	int32_t acc;
	int32_t frein;

	//entrees
	int32_t start;
	int32_t Dproxi;
	int32_t Dradar;

	// variables internes 
	int process_run;
	int process_end;
	int defaut_envi;
	int defaut_proxi;

}data;
data Data;

void cclOnDllLoad()
{
	cclSetMeasurementPreStartHandler(&OnMeasurementPreStart);
	cclSetMeasurementStartHandler(&OnMeasurementStart);
	cclSetMeasurementStopHandler(&OnMeasurementStop);
}

/* evenement d'initialisation des variables d'environnement*/
void OnMeasurementPreStart()
{
	gSysVarID_start  = cclSysVarGetID("entrees::start_park_assist");
	gSysVarID_Dproxi = cclSysVarGetID("entrees::var_distance_proxi");
	gSysVarID_Dradar = cclSysVarGetID("entrees::var_distance_radar");

	gSysVarID_volant = cclSysVarGetID("sorties::angle_volant");
	gSysVarID_acc    = cclSysVarGetID("sorties::pression_acc");
	gSysVarID_frein  = cclSysVarGetID("sorties::etat_frein");
	gSysVarID_etat   = cclSysVarGetID("sorties::etat_systeme");
	gSysVarID_duree  = cclSysVarGetID("sorties::duree_manoeuvre");

	gTimerID1 = cclTimerCreate(&OnTimer1);
	gTimerID2 = cclTimerCreate(&OnTimer2);

	cclSysVarSetHandler(gSysVarID_start, &OnSysVar_start);
	cclSysVarSetHandler(gSysVarID_Dproxi, &OnSysVar_Dproxi);
	cclSysVarSetHandler(gSysVarID_Dradar, &OnSysVar_Dradar);
}
/* evenement de lecture du start stop*/
void OnSysVar_start(int64_t time, int32_t sysVarID) {
	int32_t rc = cclSysVarGetInteger(gSysVarID_start, &Data.start);
	if (Data.start == 1) {
		pthread_create(&ptid_start, NULL, &systeme_state, &Data); // pull system_state thread
	}
	else {
		pthread_join(ptid_start, NULL);
	}
}

/* evenement qui lie les donn�es Ouputs et met � jour les variables d'envirionnement*/
void OnTimer1(int64_t time, int32_t timerID)
{
	int32_t rc;
	rc = cclSysVarSetInteger(gSysVarID_volant,Data.volant);
	rc = cclSysVarSetInteger(gSysVarID_duree,Data.duree);
	rc = cclSysVarSetInteger(gSysVarID_acc,Data.acc);
	rc = cclSysVarSetInteger(gSysVarID_frein,Data.frein);
	rc = cclSysVarSetInteger(gSysVarID_etat,Data.etat);

	cclTimerSet(gTimerID1, cclTimeMilliseconds(500));
}

/* evenement qui lie les donn�es Ouputs et met � jour les variables d'envirionnement*/
void OnTimer2(int64_t time, int32_t timerID) {

	cclPrintf("#################################");
	cclPrintf("start %i", Data.start);
	cclPrintf("Dproxi %i", Data.Dproxi);
	cclPrintf("Dradar %i", Data.Dradar);

	cclPrintf("process_run %i", Data.process_run);
	cclPrintf("process_end %i", Data.process_end);
	cclPrintf("defaut_envi %i", Data.defaut_envi);
	cclPrintf("defaut_proxi %i", Data.defaut_proxi);

	cclPrintf("duree %i", Data.duree);
	cclPrintf("etat %i", Data.etat);
	cclPrintf("volant %i", Data.volant);
	cclPrintf("acc %i", Data.acc);
	cclPrintf("frein %i", Data.frein);

	if (Data.etat != 3) {
		cclTimerSet(gTimerID2, cclTimeMilliseconds(2000));
	}
}

/* evenement de lecture de la distance proxi*/
void OnSysVar_Dproxi(int64_t time, int32_t sysVarID){
	int32_t rc = cclSysVarGetInteger(gSysVarID_Dproxi, &Data.Dproxi);
}

/* evenement de lecture de la distance radar*/
void OnSysVar_Dradar(int64_t time, int32_t sysVarID){
	int32_t rc = cclSysVarGetInteger(gSysVarID_Dradar, &Data.Dradar);
}

/* thread de detection des obstacles a proximiter du vehicule*/
void* detect_proxi(data* Data) {	
	while (Data->etat <3) {
		// distance proxi inferieur a la limite et defaut non detecter precedemment 50cm
		if (Data->Dproxi < 50) {			
			Data->defaut_proxi = 1;			
		}
		else { // distance proxi superieur a la limite et defaut detecter precedemment
			Data->defaut_proxi = 0;
		}
		Sleep(THREAD_SLEEP);
	}
	return NULL;
}

/* thread de detection des obstacles environnant (envi*) du vehicule*/
void* detect_envi(data* Data) {

	while (Data->etat < 3) {
		if (Data->Dradar < 5) { // distance proxi inferieur a la limite et defaut non detecter precedemment 5m
			Data->defaut_envi = 1;
		}
		else { // distance proxi superieur a la limite et defaut detecter precedemment
			Data->defaut_envi = 0;
		}
		Sleep(THREAD_SLEEP);
	}
	return NULL;
}

/* thread de commande des sorties (angle volant, pression acc, pression frein) */
void* process(data* Data) {

	int32_t count_timer = 0;
	int32_t max_timer = 10;

	while ((count_timer < max_timer) && (Data->etat <3) ) {

		if (Data->process_run == 1) {

			//actualiser(angle_volant,cout_timer);
			Data->volant = 45;
			Data->acc = 20;
			Data->frein = 0;

			count_timer= count_timer+1;
			Data->duree = count_timer*10 ;

			Sleep(PROCESS_SLEEP*100); // faire un define en fonction  de l'OS
		}
		else
		{
			//actualiser(angle_volant,cout_timer);
			Data->volant = 0;
			Data->acc = 0;
			Data->frein = 1;
		}
		
		Sleep(THREAD_SLEEP);		
	}

	Data->process_run = 0;
	Data->process_end = 1;

	return NULL;
}

/* thread principale calcul de l'etat du systeme */
void* systeme_state(data* Data) {

	// Demarrage des threads IO et Manoeuvre
	pthread_t ptid[3];
	pthread_create(&ptid[0], NULL, &detect_envi, Data);
	pthread_create(&ptid[1], NULL, &detect_proxi, Data);
	pthread_create(&ptid[1], NULL, &process, Data);

	cclWrite("Systeme start");

	while (!Data->process_end && Data->start) {
		if (Data->defaut_envi || Data->defaut_proxi)
		{
			Data->etat = 1; // mettre etat systeme en pause
			Data->process_run = 0; // stop manoeuvre
		}
		else if (!Data->process_end) {
			Data->etat = 2; // mettre etat systeme encours
			Data->process_run = 1; // continue manoeuvre
		}
		Sleep(THREAD_SLEEP);
	}
	Data->etat = 3; // mettre etat systeme fin

	 // arr�ter le process en cas d'interruption
	if (!Data->start) { 
		Data->process_end = 1; 
	}


	pthread_join(ptid[0], NULL);
	pthread_join(ptid[1], NULL);
	pthread_join(ptid[2], NULL);

	// reinitialisation des donnees
	data Data_init = { 0,3,0,0,0,0,100,10,0,0,0,0}; 
	*Data = Data_init;

	cclWrite("Systeme stop");

	return NULL;
}

/* initialisation des variables d'environnement */
void init_variables(data* Data) {
	// initialisation des donnees
	data Data_init = { 0,0,0,0,0,1,100,10,1,0,0,0};
	*Data = Data_init;
}

/* evenement lance en debut de simulation*/
void OnMeasurementStart(){

	init_variables(&Data);
	cclTimerSet(gTimerID1, cclTimeMilliseconds(500));  // set output refresh timer
	cclTimerSet(gTimerID2, cclTimeMilliseconds(2000));  // set output refresh timer

	//pthread_create(&ptid_start, NULL, &systeme_state, &Data); // pull system_state thread
}

/* evenement lance en fin de simulation*/
void OnMeasurementStop()
{
	pthread_join(ptid_start, NULL);
}



